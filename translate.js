const cheerio = require('cheerio');
const fs = require('fs-extra');
const path = require('path');
const he = require('he');

if (!process.argv[2]) {
  console.error('Укажите подпапку в папке files');
  process.exit();
}

files_path = path.join(__dirname, 'files', process.argv[2]);

let html = fs.readFileSync(path.join(files_path, 'output_for_translate.html'));
let text = fs.readFileSync(path.join(files_path, 'translation.txt')).toString();

const $ = cheerio.load(html);

let rows = {};
for (let row of text.split('\n\n')) {
  try {
    let num = row.split(' | ')[0];
    let row_text = row.split(' | ')[1];
    rows[num] = row_text;
  } catch(e) {
    console.log('Syntax error on the row: ', row);
  }
}

// console.log(rows);

$('[data-translate]').each(function() {
  let num = $(this).attr('data-translate');
  let row_text = rows[num];
  $(this).replaceWith(row_text);
})

fs.writeFileSync(path.join(files_path, 'translated.html'), he.decode($.html()));
